# Endless Dungeons

## Build

```bash
cmake .
make
```

## Documentation

* [Manual](docs/The Endless Dungeons.pdf)
* [Release Notes](Readme.txt). See also : [Upgrade v1.02 to 1.10](upgrade.txt)
* [Gameplay Tips](gameplay tips.txt)
* [Keyboard Shortcuts](keyboard shortcuts.txt)
