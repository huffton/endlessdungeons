Upgrading from v1.02 to 1.10:

WARNING!
Previous savegame files are not compatible and will need to be upgraded first before you can use them!

I suggest the following process to upgrade from v1.02 to v1.10 and continue a previous save:

1. Extract TheEndlessDungeons110.zip to a new directory
2. Copy your existing "Saves" folder from your previous installation into the new directory
3. Run the included executable "upgrade-saves" to upgrade your old savegame files to the new format
4. Now, run the Endless Dungeons EXE and play as usual